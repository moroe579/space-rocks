{
    "id": "64a7b1b5-2fd2-4126-8f0a-1f81ee66b62f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "334b2a82-c0cc-46a7-b904-5e23649b9a30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64a7b1b5-2fd2-4126-8f0a-1f81ee66b62f"
        },
        {
            "id": "95e659ed-0147-4185-a1af-ece5f9eba9a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c3d3a5f-ebf5-4cc4-bfbb-227eec7ffff8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64a7b1b5-2fd2-4126-8f0a-1f81ee66b62f"
        },
        {
            "id": "cbd4f8e1-6f4d-4be6-b9e4-2b8ac8a104ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "64a7b1b5-2fd2-4126-8f0a-1f81ee66b62f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1ee6b5d-cc66-4cd8-ac45-6ac4a12d2e00",
    "visible": true
}