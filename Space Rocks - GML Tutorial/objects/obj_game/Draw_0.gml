
// You can write your code in this editor
switch (room)
{
case rm_game:

	draw_text(20, 20, "SCORE: " + string(score));
	for (var i = 0; i < lives; i++) {
		draw_sprite(spr_lives, 0, x + 22 + i * 20, y + 45);
		
	}
	break;
	
case rm_start:
	draw_set_halign(fa_center);
	var c = c_yellow;
	draw_text_transformed_colour(room_width / 2, 100, "SPACE ROCKS", 3, 3, 0, c, c, c, c, 1);
	draw_text(room_width / 2, 200, 
	@"Score 1,000 points to win!

W: Move Up
A/D: Change Direction
Space: Shoot

>>PRESS ENTER TO START<<")
	draw_set_halign(fa_left);
	break;
	
case rm_win:
for (var i = 0; i < lives; i++) {
		draw_sprite(spr_lives, 0, room_width / 2 - 30 + i * 20, room_height / 2 + 20);
		draw_set_halign(fa_center);
	}
	draw_set_halign(fa_center);
	var c = c_lime;
	draw_text_transformed_colour(room_width / 2, 200, "YOU WON!", 3, 3, 0, c, c, c, c, 1);
		draw_text(room_width / 2, 300, "PRESS ENTER TO RESTART");
		draw_set_halign(fa_left);

	break;
	
case rm_gameover:
	draw_set_halign(fa_center);
	var c = c_red;
if score == 0
{
	draw_text_transformed_colour(room_width / 2, 150, "ARE YOU EVEN TRYING?", 2, 3, 0, c, c, c, c, 1);
}
else if score >= 500
{
draw_text_transformed_colour(room_width / 2, 150, "AW SO CLOSE", 3, 3, 0, c, c, c, c, 1);
}
else if score <= 500
{
draw_text_transformed_colour(room_width / 2, 150, "AT LEAST YOU TRIED", 2, 3, 0, c, c, c, c, 1);
}




	draw_text(room_width / 2, 250, "FINAL SCORE: " + string(score));
	draw_text(room_width / 2, 300, "PRESS ENTER TO RESTART");
	draw_set_halign(fa_left);

	break;
}

