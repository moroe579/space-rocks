/// @description Insert description here
// You can write your code in this editor
audio_play_sound(snd_die, 1, false);
instance_destroy();
lives -= 1;

repeat(10)
{
instance_create_layer(x, y, "Instances_1", obj_debris);
}
with (obj_game)
{
alarm[1] = room_speed;
}
if lives == 2
{
object_set_sprite(obj_ship, spr_ship_2_lives)
}
if lives == 1
{
object_set_sprite(obj_ship, spr_ship_1_lives)
}