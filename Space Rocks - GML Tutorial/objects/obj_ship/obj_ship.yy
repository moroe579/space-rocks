{
    "id": "2ab63d18-1662-414f-b06f-d36e389e2f42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "c2402ca2-e0fc-466d-9284-fdcb059b86db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ab63d18-1662-414f-b06f-d36e389e2f42"
        },
        {
            "id": "504cc7c1-dc8c-4674-9ed4-855520090286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c3d3a5f-ebf5-4cc4-bfbb-227eec7ffff8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2ab63d18-1662-414f-b06f-d36e389e2f42"
        },
        {
            "id": "a560310c-7978-4bd8-ad47-297341c54441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ab63d18-1662-414f-b06f-d36e389e2f42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
    "visible": true
}