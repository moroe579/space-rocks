{
    "id": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship_1_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40e489ec-6f37-4ca9-a20c-c59329d2cdb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "a1ffce60-d4c6-4e92-a4e7-15541e008501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e489ec-6f37-4ca9-a20c-c59329d2cdb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2a2c2af-1ba2-44cb-ba3d-d26592e61ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e489ec-6f37-4ca9-a20c-c59329d2cdb5",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "b53c53eb-738b-4649-a474-be4a7add1165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "8be0fe24-0a11-4b33-957b-37f976bcd9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b53c53eb-738b-4649-a474-be4a7add1165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d319d67b-aaec-4acf-853f-3137af987a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b53c53eb-738b-4649-a474-be4a7add1165",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "afa3ac38-3b6d-4eea-b71e-0ba1614e3f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "cf4b13b5-b24a-4793-a82e-363bdcc7be0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa3ac38-3b6d-4eea-b71e-0ba1614e3f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4155c167-8280-41cb-a82e-0ad71790602c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa3ac38-3b6d-4eea-b71e-0ba1614e3f25",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "046f34e8-ae15-42da-b6a9-11acb8914dcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "086df4d9-6a8c-467e-a027-16740777a50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046f34e8-ae15-42da-b6a9-11acb8914dcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57021a6d-5386-493f-a824-b96eb92a0f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046f34e8-ae15-42da-b6a9-11acb8914dcb",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "3cf6f5b7-620c-4c53-be6e-4f9f4ce73443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "ce3cf7ce-6155-4c4d-a557-37b46583c87d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf6f5b7-620c-4c53-be6e-4f9f4ce73443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5eb6bfb-9fc8-4e3f-a14d-bd1b5a6ab651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf6f5b7-620c-4c53-be6e-4f9f4ce73443",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "e9ffd462-5b77-423f-981f-f0a8a39cad2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "0096aeb7-6f6c-4c09-a5c0-c8456f55ea20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ffd462-5b77-423f-981f-f0a8a39cad2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "144f8064-0425-41d4-9739-590d582f5c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ffd462-5b77-423f-981f-f0a8a39cad2f",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "f8d461e8-fb98-4948-920a-caf83234fbf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "6475396f-8a08-4ccd-ad7d-ae6aa86699ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d461e8-fb98-4948-920a-caf83234fbf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "456e69dc-c758-443b-b22f-ab5b3b0a2750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d461e8-fb98-4948-920a-caf83234fbf2",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        },
        {
            "id": "71cb4db0-a188-4c2a-a309-146fc42ff695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "compositeImage": {
                "id": "0ae4c6dc-b753-41a7-a1ad-16ac407b2c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71cb4db0-a188-4c2a-a309-146fc42ff695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3148c918-f2b1-4f72-b770-024fd5af0592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71cb4db0-a188-4c2a-a309-146fc42ff695",
                    "LayerId": "bbd66a24-5003-40bf-a75d-1e10ff90c633"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "bbd66a24-5003-40bf-a75d-1e10ff90c633",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ddbfb59-be10-4d3a-a5b2-69ff3c949c33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}