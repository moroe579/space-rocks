{
    "id": "d1ee6b5d-cc66-4cd8-ac45-6ac4a12d2e00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69959e93-fb54-42b7-b335-1f3aeda7cfdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1ee6b5d-cc66-4cd8-ac45-6ac4a12d2e00",
            "compositeImage": {
                "id": "159bb77f-36de-41ca-86da-a81e700fa353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69959e93-fb54-42b7-b335-1f3aeda7cfdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b817d88d-6090-4b63-ac28-40cf92093fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69959e93-fb54-42b7-b335-1f3aeda7cfdd",
                    "LayerId": "7cbc03c4-20fa-4353-bb53-101ffe905320"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "7cbc03c4-20fa-4353-bb53-101ffe905320",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1ee6b5d-cc66-4cd8-ac45-6ac4a12d2e00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}