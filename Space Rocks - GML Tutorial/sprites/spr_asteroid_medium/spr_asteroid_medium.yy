{
    "id": "7f298b64-f291-4df8-b9e7-66594502f72e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dae692de-d6c0-4bc6-bde4-3a7aedefe09b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f298b64-f291-4df8-b9e7-66594502f72e",
            "compositeImage": {
                "id": "7b55e608-eb3d-49bd-8f0d-89b4dccb4d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae692de-d6c0-4bc6-bde4-3a7aedefe09b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6022ccf3-5324-425a-84dc-87645b96c9fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae692de-d6c0-4bc6-bde4-3a7aedefe09b",
                    "LayerId": "601c6bfd-b3ab-4287-bd5b-fd3d0cec43c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "601c6bfd-b3ab-4287-bd5b-fd3d0cec43c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f298b64-f291-4df8-b9e7-66594502f72e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}