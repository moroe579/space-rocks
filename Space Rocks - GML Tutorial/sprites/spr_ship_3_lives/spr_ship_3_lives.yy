{
    "id": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship_3_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb7e60a0-05c6-4db4-9a23-3f571445eff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "7e738efc-af34-4c2b-92c7-3feb6f03560d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7e60a0-05c6-4db4-9a23-3f571445eff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e76d8b7-4276-405d-8eef-6dac3c1cf63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7e60a0-05c6-4db4-9a23-3f571445eff4",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "d0881b0e-52ba-42ac-9660-c8fd5eca8c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "9cc554dd-fa34-4e8c-a5a6-bc216fbbb4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0881b0e-52ba-42ac-9660-c8fd5eca8c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0773ea2b-ad71-4d10-9c71-ebc752d425dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0881b0e-52ba-42ac-9660-c8fd5eca8c7e",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "a81f5f33-95ea-4d18-a525-bb45cdd959b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "0f623786-5cff-4fef-b1f2-d54517cbfbd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81f5f33-95ea-4d18-a525-bb45cdd959b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc36c41-40c0-4998-8d47-b862bf58906e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81f5f33-95ea-4d18-a525-bb45cdd959b0",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "d84f0b1b-c928-4fb2-a41e-a62bd0177a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "e5581af9-4a10-439e-99c9-cb5a94bc6dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84f0b1b-c928-4fb2-a41e-a62bd0177a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68c7cf9-3126-42d7-a342-856fc885f680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84f0b1b-c928-4fb2-a41e-a62bd0177a59",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "e0113280-0af0-41ae-b8ca-5188d02d2a64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "44c05730-c933-4705-9893-811a10110137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0113280-0af0-41ae-b8ca-5188d02d2a64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cbbcec9-2b32-48c3-a473-0a086f77f97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0113280-0af0-41ae-b8ca-5188d02d2a64",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "3d59e7fa-a3f1-42fb-b8f7-8753712777aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "86bbede9-5cb5-4862-b685-a5ff1a753731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d59e7fa-a3f1-42fb-b8f7-8753712777aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00916dea-2d9f-4244-8e40-89265776e169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d59e7fa-a3f1-42fb-b8f7-8753712777aa",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "2b4c43ff-40ac-44d3-9807-35c7fd83a804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "18aa83af-b595-4fbd-bbc6-a4be9552c415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4c43ff-40ac-44d3-9807-35c7fd83a804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8bdb12-caf3-40f1-a96f-16c85abc0c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4c43ff-40ac-44d3-9807-35c7fd83a804",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        },
        {
            "id": "6e078bc2-792d-4989-bc88-e5e6063647ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "compositeImage": {
                "id": "9f2b1f50-26fc-4f13-8118-5f8afc42d8aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e078bc2-792d-4989-bc88-e5e6063647ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a30ba9-cb29-46d3-8fbc-880c15764cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e078bc2-792d-4989-bc88-e5e6063647ce",
                    "LayerId": "0fa3e8a2-00dd-451f-afd7-017f4719b25f"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "0fa3e8a2-00dd-451f-afd7-017f4719b25f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfff38d5-329a-4fa2-8a5a-dd294d22666e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}