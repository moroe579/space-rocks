{
    "id": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship_2_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b59463b-b69c-4421-9e71-b7bed6e9cd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "094d254e-7987-4497-b71a-5edb577463ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b59463b-b69c-4421-9e71-b7bed6e9cd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f8153ee-27cb-4b7a-a1ca-ab94beefda1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b59463b-b69c-4421-9e71-b7bed6e9cd5a",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "5e76bd59-e117-41f5-97aa-ffdfae0d9c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "2f06fd63-e240-4119-892a-ae6b400c98bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e76bd59-e117-41f5-97aa-ffdfae0d9c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445d4ed4-01d6-4487-87e4-e25cb93e23e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e76bd59-e117-41f5-97aa-ffdfae0d9c47",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "285694b7-26e9-4477-ba18-b05a7575814a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "b06db068-a788-41dd-80be-71f8710eeabc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285694b7-26e9-4477-ba18-b05a7575814a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0d79dd-401d-49ad-971e-8f3e0d2c7ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285694b7-26e9-4477-ba18-b05a7575814a",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "80d88790-545b-467d-b97e-b525bfa838d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "97e19f0e-8004-4fa1-a77f-7e132ccdaedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d88790-545b-467d-b97e-b525bfa838d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd80fa0-6351-427f-bb38-d728007f6451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d88790-545b-467d-b97e-b525bfa838d0",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "c075a02d-6d12-40c3-9090-26386976b68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "fe57f079-d383-4349-90b8-7650f3675dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c075a02d-6d12-40c3-9090-26386976b68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cbd075b-ccdb-4046-9cfd-db1dcac8a450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c075a02d-6d12-40c3-9090-26386976b68f",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "8ea56b9b-f832-48d1-96be-6adb8c4777e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "22bf0813-528f-4146-8d2e-f0176ce0293e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea56b9b-f832-48d1-96be-6adb8c4777e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37db9629-98e3-42c3-871e-03bce833697d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea56b9b-f832-48d1-96be-6adb8c4777e3",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "bd224788-96fd-4024-8a0a-ca11b932b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "97d8cd1c-3894-45bd-8313-be2948261f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd224788-96fd-4024-8a0a-ca11b932b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d24416a8-8fcd-4ce9-b502-16e58631a105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd224788-96fd-4024-8a0a-ca11b932b254",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        },
        {
            "id": "78cd8fea-8456-4a71-abb3-a7e166cc4d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "compositeImage": {
                "id": "712c585f-6c87-480f-9633-3d1b3c7ad75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78cd8fea-8456-4a71-abb3-a7e166cc4d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50251629-865e-4fc4-8c2f-eba451e02a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78cd8fea-8456-4a71-abb3-a7e166cc4d1b",
                    "LayerId": "bf9b98bd-5811-4ad3-937c-282a6e9d2872"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "bf9b98bd-5811-4ad3-937c-282a6e9d2872",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f07aa55-caca-46c9-99ab-ddf683e41b20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}