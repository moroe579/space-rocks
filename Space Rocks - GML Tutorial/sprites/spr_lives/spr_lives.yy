{
    "id": "2b614dc6-5040-4696-aee2-2633019f5e0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c860d7cc-2d17-46d0-a586-0e16ca5f7abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b614dc6-5040-4696-aee2-2633019f5e0c",
            "compositeImage": {
                "id": "61120c83-c347-47ad-bdb1-2f10aaa0f2a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c860d7cc-2d17-46d0-a586-0e16ca5f7abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae1dcf2e-c147-496b-81e2-01e0e4e01be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c860d7cc-2d17-46d0-a586-0e16ca5f7abc",
                    "LayerId": "cc20c7ef-0f03-4c1e-aa78-79b0d21d8d30"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 16,
    "layers": [
        {
            "id": "cc20c7ef-0f03-4c1e-aa78-79b0d21d8d30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b614dc6-5040-4696-aee2-2633019f5e0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}