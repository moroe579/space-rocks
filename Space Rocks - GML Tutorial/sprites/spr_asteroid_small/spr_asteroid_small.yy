{
    "id": "dc25b435-c0e3-4be4-bf37-de6500d6073f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36dc6a99-9ac2-4741-86c5-6410316be381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc25b435-c0e3-4be4-bf37-de6500d6073f",
            "compositeImage": {
                "id": "30847284-f828-4931-a087-343e76e6b09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36dc6a99-9ac2-4741-86c5-6410316be381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7dd883c-fb7a-4943-99d1-eeeb01c45bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36dc6a99-9ac2-4741-86c5-6410316be381",
                    "LayerId": "cc006bca-5900-4c4c-8f7c-e5dea7f3ca3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cc006bca-5900-4c4c-8f7c-e5dea7f3ca3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc25b435-c0e3-4be4-bf37-de6500d6073f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}