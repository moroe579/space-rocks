{
    "id": "783fda81-7b4c-46aa-9764-bea526feb8c1",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "035f174d-8dd5-4e22-aa4e-fb3d9fa94ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dfa58917-fcd9-4b85-89dc-4bc48476430c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 216,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e0c43947-cff3-4524-882f-01d571ca4a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 208,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0813d3fe-5ab9-489b-8cc4-8e18be055ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1545439d-86f1-4ab2-bb06-f429c3af3770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ca50763c-19d1-42af-bc14-f4c327e5dd79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6f9a8249-d5d5-43de-9a90-55cc511dd3a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "482fdbf4-7fc6-4940-835c-1e45e9c76e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5fa88fa1-566b-440b-b7a2-66609a060f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "238f3e70-9b34-4010-b7d8-7db2fe89cfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3698eb42-4eb7-4ab4-80fa-3c8906a2646f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1ed57ac4-72b4-40bc-84a8-5497dbec00a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 124,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2c589dfb-f463-4d0a-8a3b-ab24b7d8bbe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "719a2a1e-329c-487a-aa5f-b39179e1d9e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 99,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9fffe55c-5817-482e-b709-4da071ec8d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "aca08782-fce2-4239-944c-0d1677d8fd0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "11b1f6d8-8b4a-4c3d-9024-6e2ec35ce3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eb6956b9-e23f-4597-bd02-6478db8d8806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5eef8cf9-19f6-4596-85b6-fd0df7c0ae23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "18c97951-8a68-4dc2-8813-c3884fab7728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "98a3c78c-bc0a-49f1-87ee-2b89a068b488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6f388a97-084b-44d9-b61f-a5257818717f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c71be657-c6c5-4b14-874a-fd196767ee6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "11b11dcf-ef98-438f-ac69-6256f65b54f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1aeb8b94-c82d-4f45-90d7-009ca0284815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bc532b5a-4dcd-4752-89c6-5bdc203f7fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e6a08db9-70ce-4eaa-aeb7-dae8bcae639f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c0c218eb-b2c2-4c65-a906-ae40de013da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fa1d4aac-4e15-4edc-afce-2fde35cea557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8b6b2656-a2ca-4b9f-b516-3e1779385740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1f797292-aa36-4169-93af-03508ef27689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a520fad0-47e6-4e74-84fe-a6acab9dc496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8c8dffc1-4f22-439b-8423-4b8713d1efed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 170,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e275bba0-0aff-4627-8259-c6389b4de8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1d26aa22-e672-49f4-8b91-2477d915f441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 147,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0a024f1d-387b-47f0-aaf0-b9a1cfe929c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d0aed0eb-37c4-4c97-99bf-4047a6194f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e7b6e6e9-b7dc-4503-b904-45cae7dc3eb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6793c3e9-f226-4bc6-8125-edc510ec1bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "aa66debf-cafb-4b01-be2f-9c321140c5a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6f9e0283-d168-4787-a92c-3b2cc00755a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1e5fe6b7-d24b-45ad-bbad-d25b1a44c3ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b09c51df-abb5-4461-87e5-b34e035ce481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "88934408-6962-438e-93ee-4d52b945bb05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5c646558-9b89-49a4-9b18-2452db35ad0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2ac8cbe1-b600-45e5-b12b-6ed04956aa2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 13,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "86cd336c-33d1-4305-9c78-b7819cf92971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "333322db-2398-4463-9ac3-881c75be1811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5b85cd02-4754-490e-80c1-b775a56ba316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "73ad70ae-a9f6-4884-a620-32ef2dbd86bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bb799ef1-5535-480b-8842-e406248567b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5d9b923e-92ae-4645-b83d-0afc8fd72f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b9903998-af73-4a35-8458-11686c9ed55c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0677b4f4-dfe0-43e1-9d0c-1b399c5ab95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ef0eb3e6-b3d3-4938-bca8-e65b2885976f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1fefbf44-eccd-4bcb-8545-2f0fcb8f3281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2dec835a-84cf-49d6-864e-7f50fc3bd00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "87cb4100-725d-43d8-95be-961a3bf42427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f5738205-6a01-4e12-aac4-23e29f207d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6be3cd89-aca3-42d8-9639-e688b967ecab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "206ebb77-a6d2-41b3-818a-4d7dbd335088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "180f6b94-5ea0-4e7b-8c22-68cd2ed53000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "07835637-583b-4831-b8e1-b7dbcb3f955f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f0f070a3-e4b9-4fa1-a5b9-ad163cae013b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6321ee53-2ac6-4f9c-958c-94308bf7018e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "624d5180-a35a-42ae-b3cd-5c1e1b2f074c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "198cd446-ae46-4a83-9f77-b36d7c64e496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "573a4b5d-a052-44b9-8861-4b667302dd03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "33eb3597-2d21-4b62-94a1-4fa330010888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7ecbf832-4bcb-402d-8e31-ad13a9dca260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b9eacfde-6648-40fd-8b49-e029319f4c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2b3d12ba-bd8b-47e1-9a18-fd32434e6bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "484f5582-f3c0-4b4e-921b-ad806bbdcb6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "699e308a-54ea-4c18-98e6-ce389f8f1354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bf3d3968-2cf6-40c1-8eb1-bdbe9fe2e946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 218,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "663c3d7e-3f90-4be1-b1af-9d1d3672f8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "670f460d-c30b-41ee-afbf-cf01bc7e494d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0d5b387b-3726-412d-ac2b-01cfda7aa414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 189,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "46129196-7c82-4d34-83e1-211df1acd3d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "00a999f7-a259-406f-bb65-6f2869b30eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5f65ca9b-55c7-4061-a3c9-a6029c4ad52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6d6f7d69-51f8-4e62-8bf3-f1cabe2012eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "11348693-51a1-4f9e-9014-ba003bd8bbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1c44850d-4a8d-4380-b7e5-acf525922e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2269499a-2143-4149-9778-998a2e2f4d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "72c1286b-c493-482b-8a68-44ffb5c15d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "66cab53e-73e0-4df8-b42a-b1a62ba5cec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d52904d6-5edc-4f54-b041-5d3082b83e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4c551579-8ca2-4b06-a9d6-d04c104102ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "57eaaaec-0c17-4a1c-861f-626f98682418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8726abcb-a007-46b9-9be5-8fe976809f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fe2198da-dc26-433e-abb5-1547e01e17ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7890c18e-652c-480b-ab52-c546338313cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7a62372c-0ca1-4951-88b0-20b4c7e2c567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7ba476b5-0249-4a79-b2a3-7ead34c7b881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d9cbc743-d830-4664-83f9-2818690325a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 98
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}